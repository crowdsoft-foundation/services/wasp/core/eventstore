from flask import Flask, jsonify, request, redirect, render_template
import json
import os
from pbglobal.pblib.pbrsa import encrypt
from pbglobal.pblib.helpers import get_consumer_id_by_custom_id
import html
import importlib
from flask import Response, stream_with_context
import requests
import datetime
from config import Config


app = Flask(__name__)

@app.route('/getevents', methods=["GET","POST"])
def geteventsfiltered():
    from config import Config
    from db.event import Event
    consumer_id = request.headers.get("X-Consumer-Custom-Id", "UNKNOWN")
    results = Config.db_session.query(Event).filter_by(owner=consumer_id).all()
    Config.db_session.commit()


    def generate():
        yield "["
        first = True
        for data in results:
            if not first:
                yield ","
            else:
                first = False
            yield(json.dumps({
                "id": data.id,
                "kind": data.kind,
                "type": data.type,
                "payload": data.payload,
                "owner": data.owner,
                "creator": data.creator,
                "correlation_id": data.correlation_id
            }))

        yield "]"

    response = Response(stream_with_context(generate()), mimetype='text/json')
    return response


    #return jsonify({"consumer_id": consumer_id, "filter": filter_value}), 200

@app.route('/cmd/<command>', methods=["POST"])
def home_action(command):
    correlation_id = request.headers.get("X-Correlation-ID", "noCorrelationIdSet")
    apikey = request.headers.get("apikey", "")
    payload = request.json

    if not payload.get("correlation_id"):
        payload["correlation_id"] = correlation_id

    if "password" in payload.keys():
        payload["password"] = encrypt(payload["password"])
    consumer_id = request.headers.get("X-Consumer-Custom-Id", "UNKNOWN")
    gateway_consumer_id = get_consumer_id_by_custom_id(consumer_id)
    payload["owner"] = consumer_id
    payload["consumer_id"] = consumer_id
    payload["create_time"] = datetime.datetime.utcnow().strftime('%Y-%m-%d %H:%M:%S')
    if payload.get("owner_id") is None:
        payload["owner_id"] = consumer_id

    url = os.getenv("INTERNAL_ACCOUNTMANAGER_URL") + f"/get_login_by_apikey?apikey={apikey}"

    response = requests.request("GET", url, headers={"X-Consumer-Custom-Id": consumer_id}, data={})
    print("CREATOR", response.json())
    response_json = response.json()

    payload["login"] = response_json.get("login")
    payload["creator"] = response_json.get("login")
    payload["creator_consumer_name"] = request.headers.get("X-Consumer-Username", "UNKNOWN")


    module_name = "pbglobal.commands." + command
    magic_command = getattr(importlib.import_module(module_name), command)
    # Instantiate the class (pass arguments to the constructor, if needed)
    try:
        instance = magic_command.from_json(magic_command, payload)
    except Exception as e:
        return jsonify({"error": str(e)}), 400

    instance.publish()

    # cmd = Command()
    # cmd.type = command
    # cmd.payload = payload
    # cmd.correlation_id = correlation_id
    # cmd.owner = request.headers.get("X-Consumer-Custom-Id", "UNKNOWN")
    # cmd.creator = request.headers.get("X-Consumer-Username", "UNKNOWN")
    # cmd.fire()
    return jsonify({"task_id": correlation_id}), 200


@app.route('/event/history', methods=["GET"])
def event_history_action():
    from pbglobal.pblib.pbencryption import PbCrypt
    correlation_id = request.headers.get("X-Correlation-ID")
    apikey = request.headers.get("apikey", "")
    consumer_id = request.headers.get("X-Consumer-Custom-Id", "UNKNOWN")
    search = request.args.get("search")
    event_types = request.args.get("event_types")
    starttime = request.args.get("starttime", "1900-01-01 00:00:00")
    endtime = request.args.get("endtime", datetime.datetime.now().strftime("%Y%m%d%H%M%S"))

    sql = '''
            SELECT type, payload
            FROM `events`
            WHERE kind="event" AND OWNER = :owner
            AND create_time between :starttime AND :endtime
            ORDER BY id
        '''

    if event_types and len(event_types) > 0:
        event_types = tuple(event_types.split(","))

        sql = '''
                SELECT type, payload
                FROM `events`
                WHERE kind="event" AND OWNER = :owner
                AND type IN :types
                AND create_time between :starttime AND :endtime
                ORDER BY id
            '''
    results = Config.db_session.execute(sql, {"owner": consumer_id, "types": event_types, "starttime": starttime, "endtime": endtime}).fetchall()
    return_values = []
    crypto = PbCrypt(os.getenv("EVENT_CRYPTOGRAPHY_KEY"))

    for row in results:
        string = crypto.decrypt(row.payload.encode()).decode()

        if search not in string:
            continue

        payload = json.loads(string)

        if "password" in payload.keys():
            payload["password"] = "removed for security-reasons"
        del payload["kind"]

        entry = {
            "type": row.type,
            "payload": payload
        }
        return_values.append(entry)
    Config.db_session.commit()

    return jsonify({"history": return_values}), 200

@app.route('/healthz', methods=["GET"])
def health_action():
    return jsonify({"message": "OK"}), 200


@app.route('/metrics', methods=["GET"])
def metrics_action():
    return jsonify("NYI"), 200

@app.route('/network', methods=["GET"])
def network_action():
    from config import Config
    network_config = Config()
    network_config = network_config.bindings

    return jsonify(network_config), 200

@app.route('/favicon.ico', methods=["GET"])
def favicon_action():
    return redirect("/static/favicon.ico", code=302)


@app.route("/routes")
@app.route("/routes/<path:route>")
def map(route=None):
    if route is None:
        content = ""
        internal_routes=["/favicon.ico", "/network", "/metrics", "/static/<path:filename>", "/routes", "/routes/<path:route>"]
        rules = [str(rule) for rule in app.url_map.iter_rules()]
        rules.sort()
        for rule in rules:
            if str(rule) in internal_routes:
                continue
            doc_link = '<a href ="/es/routes/' + str(rule) + '">' + str(rule) + '</a><br/>'
            content += doc_link
            filename = str(rule).replace("/", "_").replace("<", "-").replace(">", "-") + ".html"
            if not os.path.exists("/src/app/templates/" + filename):
                file = open("/src/app/templates/" + filename, 'w+')
                tpl = open("/src/app/templates/route.html", "r")
                file.write(tpl.read())
                tpl.close()
                file.close()

        return render_template('routes.html', content=content)
    else:
        filename=route.replace("/", "_").replace("<", "-").replace(">", "-") + ".html"
        return render_template("_" + filename, content="Not yet documented", route=html.escape("/" + route))

@app.before_request
def before_request():
    payload = request.get_json(silent=True)
    if payload is not None:
        payload["correlation_id"] = request.headers.get("correlation_id", "")
        request.data = json.dumps(payload)

@app.after_request
def after_request(response):
    """ Logging after every request. """
    if request.full_path not in ["/metrics?", "/metrics", "/healthz", "/healthz?"]:
        data = {}
        data["account_id"] = request.headers.get("X-Consumer-Custom-Id", "UNKNOWN")
        data["username"] = request.headers.get("X-Credential-Username", "UNKNOWN")
        data["consumername"] = request.headers.get("X-Consumer-Username", "UNKNOWN")
    return response