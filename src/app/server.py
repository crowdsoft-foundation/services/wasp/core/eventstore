from main import app
from planblick.httpserver import Server
from events.event_handler import  event_handler
from pbglobal.pblib.amqp import client

from autorun import Autorun

if __name__ == '__main__':

    Autorun().post_deployment()

    name = "eventstore"
    queue = "eventstore"
    bindings = {}
    bindings['message_handler'] = ['*']
    callback = event_handler

    client.addConsumer(name, queue, bindings, callback)

    server = Server()
    server.start(flask_app=app, app_path="/es", static_dir="/static", static_path="/es/docs", port=8000)
